#pragma once
#include "FastFunction.h"
#include "HOOK.h"
static NTSTATUS(*OriginalKdpTrap)(
	IN PKTRAP_FRAME TrapFrame,
	IN PKEXCEPTION_FRAME ExceptionFrame,
	IN PEXCEPTION_RECORD ExceptionRecord,
	IN PCONTEXT ContextRecord,
	IN KPROCESSOR_MODE PreviousMode,
	IN BOOLEAN SecondChanceException) = NULL;
static KTIMER g_PassObjTimer;
static KDPC g_PassObjDpc;
static LARGE_INTEGER g_PassObjTime;
class TwoMachineDebugging
{
public:
	NTSTATUS Start_TwoMachineDebugging(PDRIVER_OBJECT pPDriverObj);
	VOID End_TwoMachineDebugging();
private:
	VOID Pass_TP_Security_Component();
	NTSTATUS Pass_TP_KdCom_Hook(PDRIVER_OBJECT pPDriverObj);
	NTSTATUS Pass_TP_anomaly();
	ULONGLONG Get_KdpTrapAddr();
	NTSTATUS HookKdpTrap(ULONGLONG KdpTrap);
	VOID Pass_TP_KdDebuggerEnabled_Set_0();
private:
	ULONGLONG m_KdpTrap;
	HOOK m_Hook;
	ULONG m_patch_size = 0;
	PVOID m_KdpTrapBytes = NULL;
	BOOLEAN m_IsHOOK = FALSE;

private:


	static NTSTATUS HookedKdpTrap(
		IN PKTRAP_FRAME TrapFrame,
		IN PKEXCEPTION_FRAME ExceptionFrame,
		IN PEXCEPTION_RECORD ExceptionRecord,
		IN PCONTEXT ContextRecord,
		IN KPROCESSOR_MODE PreviousMode,
		IN BOOLEAN SecondChanceException
	);
};

